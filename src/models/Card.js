function Card(props)
{
    return (
        <div className={props.className ? props.className : 'card'}>
            <img src={props.cardImg} alt={"Avatar"} style={{width: "100%"}} />
            <div className="container">
                <h4><b>{props.cardTitle}</b></h4> 
                <p>{props.cardDesc}</p> 
            </div>
        </div>
    );
}

export default Card;