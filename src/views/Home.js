import Card from '../models/Card'
import { 
    useEffect,
    useState,
} from 'react';

function Home()
{
    const [users, setUsers] = useState([]);

    useEffect(() => {
        fetch(`https://dummyapi.io/data/api/user?limit=10`, {
            headers: new Headers({
                "app-id": "60349db146ff8b0837d18351"
            }),
        })
        .then(response => response.json())
        .then(response =>{
            console.log(response.data);
            setUsers(response.data);
        })
        // Catch any errors we hit and update the app
        .catch(error => { console.log(error) });
    }, []);

    return (
        users.length == 0 
        ? (<div>Loading ....</div>)
        : (
            <div>
                {users.map((user) =>
                    <Card key={user.id} cardImg={user.picture} cardTitle={`${user.firstName} ${user.lastName}`}/>
                )}
            </div>
        )
    );
}

export default Home;