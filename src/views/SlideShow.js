import React, {
    useEffect,
    useRef,
    useState
} from 'react';
import '../SlideShow.css';

function SlideComponent(props)
{
    var automaticInterval;
    const containerElm = useRef(null);
    useEffect(() => {
        updateDimensions();
        window.addEventListener("resize", updateDimensions);

        return () => {
            window.removeEventListener("resize", updateDimensions);
            if (automaticInterval) clearInterval(automaticInterval);
        }
    }, []);
    useEffect(() => {
        if (props.mode === "automatic") {
            const timeout = props.timeout || 5000;

            automaticInterval = setTimeout(
                runAutomatic,
                Number.parseInt(timeout)
            );
        }
    });
    const [slideIndex, setSlide] = useState(0);
    const ratioWHArray = props.ratio.split(":");
    const ratioWH = ratioWHArray[0] / ratioWHArray[1];
    const forward = function (){
        setSlide(getNewSlideIndex(1));
    };
    const backward = function (){
        setSlide(getNewSlideIndex(-1));
    };
    const setSlideIndex = function (index){
        setSlide(index);
    };
    const getNewSlideIndex = function(step){
        const index = slideIndex;
        const numberSlide = props.input.length;

        let newSlideIndex = index + step;
        // console.log(newSlideIndex, numberSlide);
        newSlideIndex = Math.max(Math.min(newSlideIndex, numberSlide - 1), 0);

        return newSlideIndex;
    };
    const updateDimensions = function(){
        containerElm.current.style.height 
        = `${containerElm.current.offsetWidth / ratioWH}px`;
    };
    const runAutomatic = function (){
        // setSlide(getNewSlideIndex(1));
        forward();
    };

    return (
        <div className="lp-slideshow">
            <div className="container" ref={containerElm}>
            {
                props.input.map((image, index) => {
                return (
                    <div
                        key={index}
                        className={
                            `slide ${slideIndex === index ? "active" : ""}`
                        }
                    >
                        <img className="image" src={image.src} alt={image.caption} />
                        <div className="caption-text">{image.caption}</div>
                    </div>
                )
                })
            }

            <span className="prev" onClick={backward}>❮</span>
            <span className="next" onClick={forward}>❯</span>
            </div>
        </div>
    );
}

const collection = [
    { src: 'http://placehold.it/120x120&text=image1', caption: "Caption one" },
    { src: 'http://placehold.it/120x120&text=image2', caption: "Caption two" },
    { src: 'http://placehold.it/120x120&text=image3', caption: "Caption three" },
    { src: 'http://placehold.it/120x120&text=image4', caption: "Caption four" },
    { src: 'http://placehold.it/120x120&text=image5', caption: "Caption five" },
    { src: 'http://placehold.it/120x120&text=image6', caption: "Caption six" },
];

export default class SlideShow extends React.Component {
    render() {
        return (
            <div className="App">
                {/* <SlideComponent
                    input={collection}
                    ratio={`3:2`}
                    mode={`manual`}
                /> */}

                <SlideComponent
                    input={collection}
                    ratio={`3:2`}
                    mode={`automatic`}
                    timeout={`3000`}
                />
            </div>
        );
    }
}