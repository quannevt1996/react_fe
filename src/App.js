import './App.css';
import {
	BrowserRouter as Router,
	Route,
	Switch,
	Redirect
} from 'react-router-dom';
import routes from './Routes';
import React from 'react';

function App() {

	return (
		<div className="App">
			{/* <Switch location={location} key={location.pathname}>
                
            </Switch> */}
			<Router>
				<Switch>
					{routes.map((route, idx) => {
						return route.component && (
							<Route
								key={idx}
								path={route.path}
								exact={route.exact}
								name={route.name}
								render={props => (
									<route.component 
										{...props} 
									/>
								)}
								// component={<route.component />}
								// children={<route.component />}
							>
								{/* <route.component 
								/> */}
							</Route>
						)
					})}
					{/* <Redirect from="/" to={routes[0].path} /> */}
				</Switch>
			</Router>
		</div>
	);
}

export default App;
